module.exports = {
  parser: "@typescript-eslint/parser", // Specifies the ESLint parser
  parserOptions: {
    ecmaVersion: 2021, // Allows for the parsing of modern ECMAScript features
    sourceType: "module", // Allows for the use of imports
    extraFileExtensions: [".svelte"]
  },
  plugins: ["@typescript-eslint", "svelte3"],
  extends: ["plugin:@typescript-eslint/recommended"],
  rules: {},
  settings: {
    // load TypeScript as peer dependency
    "svelte3/typescript": true,
    "svelte3/ignore-styles": () => true
  },
  env: {
    es6: true,
    browser: true
  },
  overrides: [
    {
      files: ["**/*.svelte"],
      processor: "svelte3/svelte3"
    },
    {
      files: ["**/*.cjs"],
      rules: {
        "@typescript-eslint/no-var-requires": "off"
      }
    }
  ]
};
