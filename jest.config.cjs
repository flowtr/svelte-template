module.exports = {
  // babel jest esm
  moduleFileExtensions: ["js", "jsx", "ts", "json", "node", "svelte"],
  transform: {
    "^.+\\.ts$": "babel-jest",
    "^.+\\.js$": "babel-jest",
    "^.+\\.svelte$": "babel-jest"
  }
};
