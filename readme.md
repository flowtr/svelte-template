# Svelte Template

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![Linting By ESLint](https://raw.githubusercontent.com/aleen42/badges/master/src/eslint.svg)](https://eslint.org)
[![Typescript](https://raw.githubusercontent.com/aleen42/badges/master/src/typescript.svg)](https://typescriptlang.org)

This template should help get you started developing with Svelte and TypeScript along with WindiCSS.
It also provides an eslint + prettier setup. Unfortunately, Jest testing is currently broken.
However, a [Drone](https://drone.io) CI configuration file is provided to test and lint your code each time you push new code to git.
