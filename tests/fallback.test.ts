import { render, screen } from "@testing-library/svelte";
import App from "../src/App.svelte";

test("displays hello world on homepage", () => {
  render(App);
  const node = screen.queryByText("Hello World");
  expect(node).not.toBeNull();
});
