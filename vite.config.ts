import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import windiCSS from "vite-plugin-windicss";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig(() => {
  return {
    resolve: {
      alias: {
        "@": resolve(__dirname, "./src")
      }
    },
    plugins: [svelte(), windiCSS({})]
  };
});
